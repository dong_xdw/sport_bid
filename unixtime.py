from datetime import datetime
import calendar

class Unixtime(object):
    def getCurrentUnixtime(self):
        d = datetime.utcnow()
        unixtime = calendar.timegm(d.utctimetuple())
        print(unixtime)
        return unixtime