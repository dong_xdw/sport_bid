
import requests
import os,errno
from unixtime import Unixtime as Unixtime
from GameEvent import GameEvent
from MarketOdd import MarketOdd
from Odd import Odd
import json
import GamesDict

def getUnixTime():
    unixtime = Unixtime()
    currentTime = unixtime.getCurrentUnixtime()
    return currentTime

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred



class Bet3000Reader(object):
    url = ""
    src = "bwin"
    marekt_labels = {"Tip":""}
    gamesDict:GamesDict
    marekt_labels = {"3-Way":"Match Odds",
                     "Who will win the rest of playing time?":"Remaining match",
                     "Next goal": "Next goal"
                     }
    def __init__(self, gamesDict:GamesDict):
        src = "bwin"
        self.gamesDict = gamesDict
        self.games = {}

    def read_bet3000_file(self) :
        txt = ""
        srcfile = open("C:/wangx/workspace/sportwetten/bwin.txt", "r")
        txt = srcfile.read()

        return txt

    def read_bet3000_http(self):
        groupurl = "https://www.bet3000.com/graphql"
        eventsurl1 = "https://en-eventservice.bet3000.com/v1/events?sportsbook_id=0&&offset=0&rows=30"
        eventsurl2 = "https://en-eventservice.bet3000.com/v1/events?sportsbook_id=0&&offset=0&rows=30&live_status=open,suspended,future&sort=liveWWW&date_to=1532469600"

        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0'

        }
        param1data = {"query": "{categories(lang: \"en\", hasLive: true){categories{id cid path label count_live}}}"}
        param1data = json.dumps(param1data)

        # api_token = "your api token here..."
        # headers = {'Authorization': 'token %s' % api_token}

        response = requests.post(url=groupurl, json=param1data, headers=headers)
        silentremove("d:/workspace/wetten/bet3000.txt")
        b3tfile = open("d:/workspace/wetten/bet3000.txt", "x")
        b3tfile = open("d:/workspace/wetten/bet3000.txt", "w")
        # print(response.text)
        # b3tfile.write(response.text)

        response = requests.get(eventsurl1, params=None)
        # print(response.text)
        # events = json.load(response.text)
        r = response.text
        return r


    def initGameEvents(self,events):
        for eventkey in events:
            event = events[eventkey]
            ge = GameEvent(event, eventkey, "bet3000")
            gameevent = self.gamesDict.find_game(ge)
            if not None == gameevent:
                ge = gameevent
            else:
                ge = self.gamesDict.add_game_dicts(ge)
            if not None == ge:
                self.games[eventkey] = ge

    def initPreditions(predictionevents):
        global predictions
        for eventkey in predictionevents.keys():
            event = predictionevents[eventkey]
            odds = Odd(event)
            predictions[eventkey] = odds


    def initMarketEvents(self, events, gamesinfo):

        jsonPredcitions = gamesinfo["predictions"]

        for eventkey in events.keys():
            event = events[eventkey]
            gameid = event['event_id']
            game = self.games[gameid]
            if not None == game:
                marketlabel = event['label']
                st_label = self.marekt_labels[marketlabel]
                if not None == st_label:
                    market = game.markets[st_label]
                    if None == market:
                        market = MarketOdd()
                    odds = []
                    predictionskeys = event["predictions"]
                    for oddskey in predictions:
                        jsonodd = jsonPredcitions[oddskey]
                        odd_str = jsonodd['odds']
                        odds.append(float(odd_str))
                    market.predictions[self.src] = odds




    def buildData(self):
        r = self.read_bet3000_http();

        gamesinfo = json.loads(r)
        print(type(gamesinfo))
        gameevents = gamesinfo["events"]

        jsonPredcitions = gamesinfo["predictions"]
        jsonMarkets = gamesinfo["markets"]
        self.initGameEvents(gameevents)
        self.initMarketEvents(jsonMarkets)

        silentremove("d:/workspace/wetten/b3000.txt")
        b3000file = open("d:/workspace/wetten/b3000.txt", "x")
        b3000file = open("d:/workspace/wetten/b3000.txt", "a")
        for game in self.gamesDict.values():
            gstr = game.toString()
            b3000file.write(gstr)


