import requests

import os, errno
from GameEvent import GameEvent
from MarketOdd import MarketOdd
from GamesDict import GamesDict
from bs4 import BeautifulSoup
import re

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred

class TipicoReader(object):
    url = ""
    src = "tipico"


    def __init__(self, gamesDict):
        src = "tipico"
        self.gamesDict = gamesDict

    def read_tipico_file(self):
        txt = ""
        srcfile = open("C:/wangx/workspace/sportwetten/tipico.txt", "r")
        txt = srcfile.read()

        return txt

    def read_tipico_http(self):
        startUri = "https://www.tipico.de/en/live-betting/"

        headers = ('Content-type', 'application/x-www-form-urlencoded')
        response = requests.get(url=startUri)

        linestr = response.text

        return linestr


    def buildData(self):
        htmldoc = self.read_tipico_http()
        #silentremove("d:/workspace/wetten/tipicoJson.txt")
        #tipicofile = open("d:/workspace/wetten/tipicoJson.txt", "x")
        #tipicofile = open("d:/workspace/wetten/tipicoJson.txt", "a", encoding="utf-8")

        soup = BeautifulSoup(htmldoc,features="html.parser")
        itemscopes = soup.find_all(itemtype=True)
        print(itemscopes.__len__())

        for item in itemscopes:
            self.readOneGame(item)



    def readOneGame(self, item):
        game = GameEvent()
        gameDiv = item.parent
        if ("e_active" in gameDiv['class'] and "jq-event-row-cont" not in gameDiv['class']):
            metas = item.find_all("meta")
            for meta in metas:
                if meta['itemprop'] == "name":
                    game.label_en = meta['content']
                elif  meta['itemprop'] == "description":
                    game.description = meta['content']
                elif  meta['itemprop'] == "endDate":
                    game.expires_ts = meta['content']



            gameeventdiv= gameDiv.find_all(id=re.compile("jq-event-id-"))[0]
            gameodds = gameeventdiv.find_all("div", class_=re.compile("c_3"))

            if 'Football' in game.description:
                game.type = "Football"
                self.gamesDict.footballs[game.label_en] = game
                self.createFootballMarkets(game, gameodds)
            elif "Tennis" in game.description:
                game.type = "Tennis"
                self.gamesDict.tennis[game.label_en] = game
                self.createTennisMarkets(game, gameodds)
            elif "Basketball" in game.description:
                game.type = "Basketball"
                self.gamesDict.basketballs[game.label_en] = game
                self.createBasketballMarkets(game, gameodds)
            elif "Ice Hockey" in game.description:
                game.type = "Ice Hockey"
                self.gamesDict.icehockeys[game.label_en] = game
                self.createIceHockeyMarkets(game, gameodds)
            elif "Volleyball" in game.description:
                game.type = "Volleyball"
                self.gamesDict.volleyballs[game.label_en] = game
                self.createVolleyballMarkets(game, gameodds)
            else:
                return




    def create2WayMarket(self, game, gameodds, index, label):
        marketodddiv = gameodds[index]
        marketOdd = MarketOdd()
        marketOdd.label = label
        buttondivs = marketodddiv.find_all("button")
        bvalid = True
        buttondiv0 = None
        buttondiv2 = None
        if len(buttondivs) == 3:
            buttondiv0 = buttondivs[0]
            buttondiv2 = buttondivs[2]
        elif len(buttondivs) == 2 :
            buttondiv0 = buttondivs[0]
            buttondiv2 = buttondivs[1]

        if 'c_but_off' in buttondiv0['class'] or 'c_but_off' in buttondiv2['class']:
            bvalid = False
        if (bvalid):
            predictions = [None] * 2
            marketOdd.predictions[self.src] = predictions
            prediction00Id = buttondivs[0]['result_pk']
            predictions[0] = float(buttondiv0.text.strip().replace(',' , '.'))
            predictions[1] = float(buttondiv2.text.strip().replace(',' , '.'))
            game.markets[label] = marketOdd


    def createTennisMarkets(self,game, gameodds):
        self.create2WayMarket(game, gameodds, 0, "Match odds")
        self.create2WayMarket(game, gameodds, 1, "Set")
        self.createPointsMarket(game, gameodds, 3, "Sets overall")

    def createTennisUpcoming(self, game, gameodds):
        self.create2WayMarket(game, gameodds, 0, "Match odds")
        self.create2WayMarket(game, gameodds, 1, "HT/Set/Third")
        self.createPointsMarket(game, gameodds, 2, "Over / Under")

    def createBasketballUpcoming(self,game, gameodds):
        self.create2WayMarket(game, gameodds, 0, "Match odds")
        self.create3WayMarket(game, gameodds, 1, "HT/Set/Third")
        self.createPointsMarket(game, gameodds, 2, "Over / Under")

    def createBasketballMarkets(self, game, gameodds):
        self.create2WayMarket(game, gameodds, 0, "Match odds")
        self.create3WayMarket(game, gameodds, 1, "Half")
        self.create2WayMarket(game, gameodds, 2, "Handicap")
        self.createPointsMarket(game, gameodds, 3, "Points")

    def createVolleyballUpcoming(self, game, gameodds):
        self.create2WayMarket(game, gameodds, 0, "Match odds")
        self.create2WayMarket(game, gameodds, 1, "HT/Set/Third")
        self.createPointsMarket(game, gameodds, 2, "Over / Under")

    def createVolleyballMarkets(self, game, gameodds):
        self.create2WayMarket(game,gameodds, 0, "Match odds")
        self.create2WayMarket(game, gameodds, 1, "Set")
        self.createPointsMarket(game, gameodds, 3, "Points")


    def creatIceHockeyUpcoming(self, game, gameodds):
        self.create3WayMarket(game, gameodds, 0, "Match odds")
        self.createPointsMarket(game, gameodds, 2, "Over / Under")

    def createIceHockeyMarkets(self, game, gameodds):
        self.create3WayMarket(game, gameodds, 0, "Match Odds")
        self.create3WayMarket(game, gameodds, 1, "Period winner")
        self.create3WayMarket(game, gameodds, 2, "Next goal")
        self.createPointsMarket(game, gameodds, 3, "Over/Under")

    def createVolleyballUpcoming(self, game, gameodds):
        self.create2WayMarket(game, gameodds, 0, "Match odds")
        self.create2WayMarket(game, gameodds, 1, "HT/Set/Third")

    def createVolleyballMarkets(self, game, gameodds):
        self.create2WayMarket(game,gameodds, 0, "Match odds")
        self.create2WayMarket(game, gameodds, 1, "Set")
        self.createPointsMarket(game, gameodds, 3, "Points")

    def createHandballMarkets(self, game, gameodds):
        self.create3WayMarket(game, gameodds, 1, "Match Odds")
        self.create2WayMarket(game, gameodds, 2, "Half")
        self.createPointsMarket(game, gameodds, 3, "Handicap")
        self.createPointsMarket(game, gameodds, 3, "Goals overall")


    def createPointsMarket(self, game, gameodds, index, label):

        marketodddiv = gameodds[index]
        marketOdd = MarketOdd();
        marketOdd.label = label
        buttondivs = marketodddiv.find_all("button")
        bvalid = True
        for buttondiv in buttondivs:
            if 'c_but_off' in buttondiv['class']:
                bvalid = False
                break
        if (bvalid):
            predictions = [None] * 2
            marketOdd.predictions[self.src] = predictions
            typediv = marketodddiv.find_all(class_='type')[0]
            spandiv = typediv.find_all("span")[0]
            marketOdd.type = spandiv.text
            predictions[0] = float(buttondivs[0].text.strip().replace(',' , '.'))
            predictions[1] = float(buttondivs[1].text.strip().replace(',' , '.'))
            game.markets[label] = marketOdd


    def create3WayMarket(self, game, gameodds, index, label):
        marketodddiv = gameodds[index]
        marketOdd = MarketOdd()
        marketOdd.label = label
        buttondivs = marketodddiv.find_all("button")
        bvalid = True
        for buttondiv in buttondivs:
            if 'c_but_off' in buttondiv['class']:
                bvalid = False
                break
        if (bvalid):
            odds = [None]*3
            marketOdd.predictions[self.src] = odds
            prediction00Id = buttondivs[0]['result_pk']
            try:
                odds[0] = float(buttondivs[0].text.strip().replace(',' , '.'))
                odds[1] = float(buttondivs[1].text.strip().replace(',' , '.'))
                odds[2] = float(buttondivs[2].text.strip().replace(',' , '.'))
                game.markets[label] = marketOdd
            except IndexError:
                print (len(predictions))


    def createFootballMarkets(self, game, gameodds):
        self.create3WayMarket(game, gameodds, 0, "Match Odds")
        self.create3WayMarket(game, gameodds, 1, "Remaining match")
        self.create3WayMarket(game, gameodds, 2, "Next goal")
        self.createPointsMarket(game, gameodds, 3, "Goals frm now")










