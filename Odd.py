
class Odd:
    market_id = ""
    odds = ""
    label = ""
    lable_short = ""
    type = ""
    probability = ""

    def __init__(self, oddInfo):
        self.market_id = oddInfo["market_id"]
        self.odds = oddInfo["odds"]
        self.type = oddInfo["type"]
        self.label = oddInfo["label"]
        self.label_short = oddInfo["label_short"]
        if "probability" in oddInfo:
            self.probability = oddInfo["probability"]
