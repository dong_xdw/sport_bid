
import http.client
import urllib

import requests
import os, errno
import json
import codecs
from unixtime import Unixtime as Unixtime
from matchgroup import MatchGroup
from GameEvent import GameEvent
from MarketOdd import MarketOdd
from GamesDict import GamesDict
from Odd import Odd
from ReadInterwetten import InterwettenReader
from ReaderBetAtHome import BetAtHomeReader
from match import Matchgame
from ReaderTipico import TipicoReader
###
#bah: bet-at-home
###

groups = {}
games = {}
markets = {}
predictions = {}

bah_ids = {}


def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred

def getMatchGroup(oddInfo):
    global groups
    mg = MatchGroup()
    if (oddInfo[2] not in groups.keys()):
        mg.gid = oddInfo[1]
        mg.gname = oddInfo[2]
        mg.gnation = oddInfo[3]
        mg.gtype = oddInfo[6]
        groups.append(mg.gname, mg)
    return mg

def getMatchEvents(info):
    global games
    game = GameEvent()
    if (info[1] not in games.keys()):
        game.player1 = info[13]
        game.palyer2 = info[14]
        game.id1 = info[1]
        games.append(game.id1, game)
    return game

def initGameEvents(events):
    global games
    for eventkey in events:
        event = events[eventkey]
        ge = GameEvent(event,eventkey, "bet3000")
        games[ge.id] = ge

def initMarketEvents(events):
    global markets
    global predictions
    for eventkey in events.keys():
        event = events[eventkey]
        predictionskeys = event["predictions"]
        eventpredictions = {key:predictions[key] for key in predictionskeys}
        market = MarketOdd(eventkey, event,eventpredictions);
        markets[eventkey] = market

def initPreditions(predictionevents):
    global predictions
    for eventkey in predictionevents.keys():
        event = predictionevents[eventkey]
        odds = Odd(event)
        predictions[eventkey] = odds


def handleInfo(type, oddInfo):
    odddict = {
        'G': lambda: getMatchGroup(oddInfo),
        'E': lambda: getMatchGroup(oddInfo)
    }

def getUnixTime():
    unixtime = Unixtime()
    currentTime = unixtime.getCurrentUnixtime()
    return currentTime

def readbet3000():
    global markets
    global predictions
    global games
    groupurl = "https://www.bet3000.com/graphql"
    eventsurl1 = "https://en-eventservice.bet3000.com/v1/events?sportsbook_id=0&&offset=0&rows=30"
    eventsurl2 = "https://en-eventservice.bet3000.com/v1/events?sportsbook_id=0&&offset=0&rows=30&live_status=open,suspended,future&sort=liveWWW&date_to=1532469600"

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0'

    }
    param1data = {"query": "{categories(lang: \"en\", hasLive: true){categories{id cid path label count_live}}}"}
    param1data = json.dumps(param1data)


    #api_token = "your api token here..."
    #headers = {'Authorization': 'token %s' % api_token}

    response = requests.post(url=groupurl, json=param1data, headers=headers)
    silentremove("d:/workspace/wetten/bet3000.txt")
    b3tfile = open("d:/workspace/wetten/bet3000.txt", "x")
    b3tfile = open("d:/workspace/wetten/bet3000.txt", "w")
    #print(response.text)
    #b3tfile.write(response.text)

    response = requests.get(eventsurl1, params=None)
    #print(response.text)
    #events = json.load(response.text)
    r = response.text
    b3tfile.write(response.text)
    gamesinfo = json.loads(r)
    print(type(gamesinfo))
    gameevents = gamesinfo["events"]

    jsonPredcitions = gamesinfo["predictions"]
    jsonMarkets = gamesinfo["markets"]
    initPreditions(jsonPredcitions)
    initMarketEvents(jsonMarkets)
    initGameEvents(gameevents)
    silentremove("d:/workspace/wetten/b3000.txt")
    b3000file = open("d:/workspace/wetten/b3000.txt", "x")
    b3000file = open("d:/workspace/wetten/b3000.txt", "a")
    for game in games.values():
        gstr = game.toString()
        b3000file.write(gstr)

    for marketkey in markets:
        market = markets[marketkey]
        mstr = market.toprint()
        b3000file.write(mstr)





def search_games(glabel):
    global games
    for game in games.values():
        if glabel == game.label_en:
            return game
    return None

def search_bids(bidInfo, opts, foundgame):
    marketLabels = {"2-way":"half 2-Way",
                    "Tip":"3-Way",
                    "Who will win the rest of playing time":"Rest of match",
                    "1st Half Over/Under2.5":"",
                    "Which team will have corner kick ":"",
                    "How many goals will be scored":"Exact number of goals in match",
                    "How many goals will be scored in the 1st half?":"Exact number of goals in first half",
                    "Handicap (1:0)": "Handicap1:0",
                    "Handicap (0:1)": "Handicap0:1",
                    "Handicap (0:2)": "Handicap2:0",
                    "Double Chance":"Double Chance",
                    "Who will score the next goal?": "Next goal",
                    "Who will win the rest of the 1st half?":"Who wins the rest of the half time?",
                    "Which team will win the second half?":"",
                    "How many goals will the home team score?":"Goals Home Team",
                    "How many goals will the away team score?":"Goals Away Team",
                    "How many goals will the home team score? (under/over 2.5)":"Home Team Over/Under2.5",
                    "How many goals will the away team score? (under/over 1.5)":"Guest Team Over/Under1.5"
                    }
    global markets
    global games
    bidLabel = bidInfo[3]
    for labelkey in marketLabels:
        if labelkey in bidLabel:
            marketlabel = marketLabels[labelkey]
            marketIds = foundgame.markets
            for marketId in marketIds:
                market = markets[marketId]
                mlabel = market.label
                if marketlabel == mlabel:
                    print(foundgame.label_en, mlabel)


            





def read_bet_at_home():
    games_bah = {}
    odds_bah = {}
    pdcs_bah = {}

    tennisLabels = {"Which player will win the match?":"2-way",
                    "Which player will win set 2?": "2-way",
                    }
    global bah_ids
    global games

    startUri = "https://www.bet-at-home.com/svc/bah/GetUserBalance"
    currentTime = getUnixTime()
    restUri = "https://www.bet-at-home.com/svc/livebet/data?lang=EN&jid=1&_=" + str(currentTime)

    req = urllib.request.Request(url=restUri,  method='GET')
    req.add_header('Content-type', 'application/x-www-form-urlencoded')
    r = urllib.request.urlopen(req).read()
    linestr = r.decode('utf8')
    lines = linestr.splitlines()

    silentremove("d:/workspace/wetten/bet-at-home.txt")
    bahfile = open("d:/workspace/wetten/bet-at-home.txt", "x")
    bahfile = open("d:/workspace/wetten/bet-at-home.txt", "a",encoding="utf-8")

    lIndex = 0
    lsize = len(lines)

    while lIndex < lsize:
        line = lines[lIndex]
        attributes = line.split('|')
        attrStr = ""
        bahfile.write("\n")
        for idx, val in enumerate(attributes):
            attrStr += str(idx) + ": " + val+ " | "
        if (attributes[0] == "E"):
            games_bah[attributes[1]] = attributes
            glabel_en = generateLabelEn(attributes)
            #if glabel_en == "Cruz Azul - Club Leon":
            #    print("stop:" + glabel_en)
            foundgame = search_games(glabel_en)
            if foundgame != None:
                print(foundgame.label_en)
                gameId = attributes[1]
                bah_ids[gameId] = foundgame.id

                bidIndex = lIndex+1
                bidLine = None
                if (bidIndex < lsize):
                    bline = lines[bidIndex]
                    bidLine = bline.split('|')
                if (bidLine[0] == "B" or bidLine[0] == "P"):
                    while bidIndex < lsize and bidLine[1] == gameId:
                        if bidLine[0] == "B":
                            bidId = bidLine[2]
                            bidlabel = bidLine[3]
                            bidPreNum = bidLine[5]
                            bidOpt = []
                            if bidPreNum == 2:
                                bidOpt[0] = float(bidLine[6])
                                bidOpt[1] = float(bidLine[8])
                            elif bidPreNum == 3:
                                bidOpt[0] = float(bidLine[6])
                                bidOpt[1] = float(bidLine[7])
                                bidOpt[2] = float(bidLine[8])
                            elif bidPreNum == 1:
                                bidIndex += 1;
                                bidOptLine = lines[bidIndex]
                                opts = bidOptLine.split('|')
                                optIndex = 0
                                while bidIndex < lsize and opts[2] == bidId and opts[0] == "P":
                                    bidOpt[optIndex] = opts[5]
                                    optIndex += 1
                                    bidIndex += 1
                                    if (bidIndex < lsize):
                                        bidOptLine = lines[bidIndex]
                                        opts = bidOptLine.split('|')
                                if opts[2] != bidId or opts[0] != "P":
                                    bidIndex -= 1
                        bidIndex += 1
                        if (bidIndex < lsize):
                            bline = lines[bidIndex]
                            bidLine = bline.split('|')
                        lIndex = bidIndex
                    search_bids(bidLine, bidOpt, foundgame)
                else:
                    lIndex = bidIndex
            else:
                lIndex += 1
        else:
            lIndex += 1
        #if (attrStr[0] == "B"):
        #    odds_bah.append(attributes[1],attributes)
        #if (attrStr[0] == "P"):
        #    pdcs_bah.append(attributes[1],attributes)
        bahfile.write(line)


    #for gevent in games.values():
     #   gname_en = gevent[15] +


def generateLabelEn(attributes):
    glabel_en = attributes[15]
    bWomen = False
    bU20 = False
    if ("U20" in attributes[13]):
        bU20 = True
    if ("Women" in attributes[12] or "Women" in attributes[13]):
        bWomen = True
    glabel_en += " "
    if bU20:
        glabel_en += "U20 "
    if bWomen:
        glabel_en += "(F) "
    glabel_en += "- "
    glabel_en += attributes[16]
    if bU20:
        glabel_en += " U20"
        # print(glabel_en)
    if bWomen:
        glabel_en += " (F)"
        # print(glabel_en)
    return glabel_en


if __name__ == "__main__":
    #readbet3000()
    #read_bet_at_home()
    #interwettenReader = InterwettenReader()
    #iwresponse = interwettenReader.buildData()
    #betAtHomeReader = BetAtHomeReader()
    #betAtHomeReader.buildData()
    gamesDict = GamesDict()
    tipicoreader = TipicoReader(gamesDict)
    tipicoreader.buildData()

    betathome_reader = ReaderBetAtHome(gamesDict)


    silentremove("d:/workspace/wetten/games.txt")
    gamesfile = open("d:/workspace/wetten/games.txt", "x")
    gamesfile = open("d:/workspace/wetten/games.txt", "a", encoding="utf-8")

    for game in gamesDict.footballs.values():
        gstr = game.toJson()
        gamesfile.write(gstr)
    for game in gamesDict.basketballs.values():
        gstr = game.toJson()
        gamesfile.write(gstr)
    for game in gamesDict.volleyballs.values():
        gstr = game.toJson()
        gamesfile.write(gstr)
    for game in gamesDict.tennis.values():
        gstr = game.toJson()
        gamesfile.write(gstr)





