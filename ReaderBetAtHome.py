
import requests

import os
import errno
from unixtime import Unixtime as Unixtime
from GameEvent import GameEvent
from MarketOdd import MarketOdd
from GamesDict import GamesDict


def getUnixTime():
    unixtime = Unixtime()
    currentTime = unixtime.getCurrentUnixtime()
    return currentTime

class BetAtHomeReader(object):
    url = ""
    src = "betathome"
    games = {}
    markets = {}
    marekt_labels = {"Tip":"Match Odds",
                     "Who will win the rest of playing time?":"Remaining match",
                     "Who will score the next goal?": "Next goal",
                     "Who will lead at halftime?":"Who will lead at halftime?"}

    def __init__(self, gamesDict:GamesDict):
        src = "bet_at_home"
        self.gamesDict = gamesDict


    def read_betathome_file(self) :
        txt = ""
        srcfile = open("C:/wangx/workspace/sportwetten/interwetten.txt", "r")
        txt = srcfile.read()

        return txt

    def read_betathome_http(self):

        currentTime = getUnixTime()
        restUri = "https://www.bet-at-home.com/svc/livebet/data?lang=EN&jid=1&_=" + str(currentTime)

        headers= ('Content-type', 'application/x-www-form-urlencoded')
        response = requests.get(url=restUri)

        linestr = response.text

        return linestr

    def generateLabelEn(self, attributes, game:GameEvent):
        glabel_en = attributes[15]
        gametype = attributes[11]
        bWomen = False
        bU20 = False
        if ("U20" in attributes[13]):
            bU20 = True
        if ("Women" in attributes[12] or "Women" in attributes[13]):
            bWomen = True
        glabel_en += " "
        #if bU20:
         #   glabel_en += "U20 "
        if bWomen:
            glabel_en += "(F) "
        glabel_en += "- "
        glabel_en += attributes[16]
        #if bU20:
         #   glabel_en += " U20"
            # print(glabel_en)
        if bWomen:
            glabel_en += " (F)"
        game.label_en = glabel_en
        matchedgame =  self.gamesDict.find_game(game)
        if matchedgame == None:
            matchedgame = game
            matchedgame = self.gamesDict.add_global_game(game, gametype)
        return matchedgame



    def silentremove(self,filename):
        try:
            os.remove(filename)
        except OSError as e:  # this would be "except OSError, e:" before Python 2.6
            if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
                raise  # re-raise exception if a different error occurred

    def buildData(self):
        linestr = self.read_betathome_http()
        lines = linestr.splitlines()
        lIndex = 0
        lsize = len(lines)

        self.silentremove("d:/workspace/wetten/bet-at-home.txt")
        bahfile = open("d:/workspace/wetten/bet-at-home.txt", "x")
        bahfile = open("d:/workspace/wetten/bet-at-home.txt", "a", encoding="utf-8")

        while lIndex < lsize:
            line = lines[lIndex]

            bahfile.write(line+"\n")

            attributes = line.split('|')
            bidIndex = lIndex + 1
            if (attributes[0] == "E"):
                #games_bah[attributes[1]] = attributes
                game = GameEvent()
                game.src = self.src
                gameOrgId = attributes[1]
                game.id = attributes[1] + "_" + self.src
                game.type = attributes[11]
                game = self.generateLabelEn(attributes,game)
                if None == game:
                    pass
                else:
                    glabel_en = game.label_en
                    self.games[glabel_en]= game


                    #read markets

                    if bidIndex < lsize:
                        marketline = lines[bidIndex]
                        marketInfo = marketline.split('|')

                    if (attributes[1] == "11717894"):
                        print(game.label_en)
                    while bidIndex < lsize and (marketInfo[1] == gameOrgId):
                        if marketInfo[0] == "B":
                            mlabel = marketInfo[3]
                            mlabel = self.find_market_label(mlabel)
                            if None == mlabel:
                                pass
                            else:
                                #if not None == mlabel:
                                market:MarketOdd = game.markets[mlabel]
                                if None == market:
                                    market = MarketOdd()
                                    if not None == mlabel:
                                        game.markets[mlabel] = market

                                #market.id = marketInfo[2] + "_" + self.src
                                marketOrgId = marketInfo[2]
                                #game.markets.append(market.id)
                                #self.markets[market.id] = market

                                market.label = mlabel
                                bidNum = int(marketInfo[5])
                                predictions = []
                                market.predictions[self.src] = predictions
                                if 2 == bidNum:
                                    predictions.append(float(marketInfo[6]))
                                    predictions.append(float(marketInfo[8]))
                                    bidIndex += 1
                                    if bidIndex < lsize:
                                        marketline = lines[bidIndex]
                                        marketInfo = marketline.split('|')
                                elif bidNum == 3:
                                    predictions.append(float(marketInfo[6]))
                                    predictions.append(float(marketInfo[7]))
                                    predictions.append(float(marketInfo[8]))
                                    bidIndex += 1
                                    if bidIndex < lsize:
                                        marketline = lines[bidIndex]
                                        marketInfo = marketline.split('|')
                                elif bidNum == 1:
                                    bidIndex += 1
                                    marketline = lines[bidIndex]
                                    marketInfo = marketline.split('|')
                                    oddindex = 0
                                    while bidIndex < lsize and marketInfo[0] == "P" and marketInfo[2] == marketOrgId:
                                        predictions.append(float(marketInfo[5]))
                                        oddindex += 1
                                        bidIndex += 1
                                        if bidIndex < lsize :
                                            marketline = lines[bidIndex]
                                            marketInfo = marketline.split('|')
                        else :
                            bidIndex += 1
                            if bidIndex < lsize:
                                marketline = lines[bidIndex]
                                marketInfo = marketline.split('|')
            lIndex = bidIndex

        for gamekey in self.games:
            game = self.games[gamekey]
            print(game.label_en+ ":")
            for marketkey in game.markets:
                market = self.markets[marketkey]
                print("---" + market.label)

    def find_market_label(self, mlabel):
        bValid = False
        for standardlabel in self.market_labels.keys:
            if standardlabel in mlabel:
                mlabel = self.market_labels[standardlabel]
                bValid = True
                break
        if not bValid:
            return None
        else:
            return mlabel