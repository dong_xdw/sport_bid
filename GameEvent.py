


class GameEvent(object):


    def __init__(self, eventInfo=None, eventid=None, source=None):
        self.label = ""
        self.label_en = ""
        self.label_de = ""
        self.id = ''
        self.score_type = ''
        self.markets = {}
        self.expires_ts = 0
        self.category_id = 0
        self.label_de = ''
        self.score = []
        self.live_status = ""
        self.src = ""
        self.description = ""
        self.cards: {"red": [], "yellow": []}
        self.type = ""
        self.state = ""
        self.league = ""
        if None != eventInfo:
            self.label = eventInfo["label"]
            self.label_en = eventInfo["label_en"]
            self.id = eventid
            self.src = source
            if "score" in eventInfo:
                if "score" in eventInfo["score"]:
                    self.score = eventInfo["score"]["score"]
            self.live_status = eventInfo["live_status"]
            #self.markets = eventInfo["markets"]
            self.category_id = eventInfo["category_id"]
            self.live_status = eventInfo["live_status"]
            jsontype:str = eventInfo['score_type']
            if jsontype.startswith("SOCCER"):
                self.type = "Football"
            elif jsontype.startswith("TENNIS"):
                self.type = "Tennis"
            elif jsontype.startswith("VOLLEYBALL"):
                self.type = "Volleyball"
            elif jsontype.startswith("BASKETBALL"):
                self.type = "Basketball"
            elif jsontype.startswith("HANDBALL"):
                self.type = "Handball"


    def toJson(self):
        json_str = "\n"
        json_str +="'" + self.label_en + "' : { \n"
        for marketkey, market in self.markets.items():
            json_str += market.toJson()
        json_str += "}\n"
        return json_str

    def toString(self):
        toStr = "{" + "'label': " + self.label + ", "
        toStr += "'label_en': " + self.label_en + ","
        toStr += "'id' :" + self.id + ","
        toStr += "'live_status': " + self.live_status + ","
        toStr += "'category_id': " + self.category_id + ","
        toStr += "'score': "
        for s in self.score:
            toStr += str(s) + " | "
        toStr += "\n"
        toStr += "}"
        #print(toStr)
        return toStr


