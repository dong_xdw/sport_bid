
import json
class MarketOdd:


    def __init__(self, id=None,  marketInfo=None, predictions=None):
        self.id = ""
        self.event_id = ""
        self.odds = []
        self.predictions = {}
        self.label = ""
        self.type = ""
        self.type_label = ""
        self.live_status = ""
        self.sites = []
        if None != marketInfo:
            self.id = id
            self.event_id = marketInfo["event_id"]
            self.label = marketInfo["label"]
            self.type = marketInfo["type"]
            self.type_label= marketInfo["type_label"]
            bet3todds = []
            self.predictions = predictions
            for pdictkey in predictions:
                prediction = predictions[pdictkey]
                strodd = prediction.odds
                odd  = float(strodd)
                bet3todds.append(odd)
            self.odds.append(bet3todds)


    def toJson(self):
        jsonstr = json.dump(self)
        return jsonstr

    def addodds(self, oddsbah):
        bahodds = []
        self.odds.append(bahodds)

    def toJson(self):
        json_str = "\n"
        json_str += "'" + self.label + "' : {\n 'Predictions': \n"
        for srckey in self.predictions:
            json_str += "'" + srckey + "':"
            odds = self.predictions[srckey]
            for odd in odds:
                json_str += " " + str(odd) + " |"
            json_str += "\n"
        json_str += "}\n"
        return json_str

    def toprint(self):
        toStr = "{'id':"+str(self.id)+"; 'event_id: '"+ str(self.event_id)+", "
        toStr += "'label': " + str(self.label) + ","
        toStr += "'type':" + str(self.type) + ", "
        toStr += "'odds': "
        for oddarray in self.odds:
            for odd in oddarray :
                toStr += str(odd) + " | "
            toStr += ""
        toStr += "}"
        #print(toStr)
        return toStr