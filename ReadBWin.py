
import requests
import os,errno
from unixtime import Unixtime as Unixtime
from GameEvent import GameEvent
from MarketOdd import MarketOdd

def getUnixTime():
    unixtime = Unixtime()
    currentTime = unixtime.getCurrentUnixtime()
    return currentTime

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred

    def generateLabelEn(attributes):
        glabel_en = attributes[3]
        bWomen = False
        bU20 = False
        if ("U20" in attributes[5]):
            bU20 = True
        if ("Women" in attributes[5]):
            bWomen = True

        indexminus = glabel_en.index(" - ")
        player1 = glabel_en[:indexminus]
        player2 = glabel_en[indexminus+3:]
        glabel_en = player1
        if bWomen:
            glabel_en += "(F)"
        glabel_en += " - "
        glabel_en += player2
        if bWomen:
            glabel_en += " (F)"
        return glabel_en

class BWinReader(object):
    url = ""
    src = "bwin"
    def __init__(self):
        src = "bwin"

    def read_bwin_file(self) :
        txt = ""
        srcfile = open("C:/wangx/workspace/sportwetten/bwin.txt", "r")
        txt = srcfile.read()

        return txt

    def read_bwin_http(self):
        startUri = "https://livebetting.bwin.com/en/live/secure/api/betoffer/liveEvents"
        currentTime = getUnixTime()

        response = requests.post(url=startUri)

        linestr = response.text

        return linestr


    def buildData(self):
        linestr = self.read_bwin_http();

        silentremove("d:/workspace/wetten/interwetten.txt")
        iwfile = open("d:/workspace/wetten/interwetten.txt", "x")
        iwfile = open("d:/workspace/wetten/interwetten.txt", "a", encoding="utf-8")
