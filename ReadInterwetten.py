
import requests
import os,errno
from unixtime import Unixtime as Unixtime
from GameEvent import GameEvent
from MarketOdd import MarketOdd
from GamesDict import GamesDict

def getUnixTime():
    unixtime = Unixtime()
    currentTime = unixtime.getCurrentUnixtime()
    return currentTime

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred

class InterwettenReader(object):
    url = ""
    src = "interwetten"
    games = {}
    markets = {}
    marekt_labels = {"Match":"Match Odds",
                     "Rest of the match":"Remaining match",
                     "Team to score": "Next goal"
                     }
    def __init__(self, gamesDict:GamesDict):
        src = "interwetten"
        self.gamesDict = gamesDict



    def read_interwetten_file(self) :
        txt = ""
        srcfile = open("C:/wangx/workspace/sportwetten/interwetten.txt", "r")
        txt = srcfile.read()

        return txt

    def read_interwetten_http(self):
        startUri = "https://www.interwetten.com/en/sportsbook/LiveBetting.iw?ALL=1&txt=1&chk=1536265243000&MODE=1&TIMEZONEOFFSET=-120"
        currentTime = getUnixTime()
        restUri = "https://www.bet-at-home.com/svc/livebet/data?lang=EN&jid=1&_=" + str(currentTime)

        headers= ('Content-type', 'application/x-www-form-urlencoded')
        response = requests.post(url=startUri)

        linestr = response.text

        return linestr

    def generateLabelEn(self, attributes, game:GameEvent):
        glabel_en = attributes[3]
        bWomen = False
        bU20 = False
        if ("U20" in attributes[5]):
            bU20 = True
        if ("Women" in attributes[5]):
            bWomen = True

        indexminus = glabel_en.index(" - ")
        player1 = glabel_en[:indexminus]
        player2 = glabel_en[indexminus+3:]
        glabel_en = player1
        if bWomen:
            glabel_en += "(F)"
        glabel_en += " - "
        glabel_en += player2
        if bWomen:
            glabel_en += " (F)"

        gametype = game.type
        matchedgame = self.gamesDict.find_game(game)
        if matchedgame == None:
            matchedgame = self.gamesDict.add_global_game(game, gametype)
        return matchedgame



    def buildData(self):
        linestr = self.read_interwetten_http();
        lines = linestr.splitlines()
        lIndex = 0
        lsize = len(lines)

        silentremove("d:/workspace/wetten/interwetten.txt")
        iwfile = open("d:/workspace/wetten/interwetten.txt", "x")
        iwfile = open("d:/workspace/wetten/interwetten.txt", "a", encoding="utf-8")

        while lIndex < lsize:
            line = lines[lIndex]
            attributes = line.split('|')
            bidIndex = lIndex + 1

            iwfile.write(line + "\n")

            if (attributes[0] == "E"):
                #games_bah[attributes[1]] = attributes
                game = GameEvent()
                game.src = self.src
                game.id = attributes[1] + "_" + self.src
                game.type = attributes[4]
                game = self.generateLabelEn(attributes, game)
                if None == game:
                    pass
                else:
                    game.label_en = glabel_en

                    self.games[glabel_en]= game

                    #read markets

                    if bidIndex < lsize:
                        marketline = lines[bidIndex]
                        marketInfo = marketline.split('|')


                    while bidIndex < lsize and (marketInfo[0] == "G" or marketInfo[0] == "O"):
                        iwfile.write(marketline + "\n")
                        if marketInfo[0] == "G":
                            marketlabel = marketInfo[3]
                            mlabel = self.find_market_label(marketlabel)
                            if None == mlabel:
                                bidIndex += 1
                                pass
                            else:
                                market = game.markets[mlabel]
                                if None == market:
                                    market = MarketOdd()
                                    market.label = mlabel
                                    game.markets[mlabel] = market

                                odds = []
                                bidIndex += 1
                                marketline = lines[bidIndex]
                                marketInfo = marketline.split('|')
                                oddindex = 0
                                market.predictions[self.src] = odds
                                while bidIndex < lsize and marketInfo[0] == "O":
                                    iwfile.write(marketline + "\n")
                                    oddoption = int(marketInfo[4]) / 100
                                    odds[oddindex] = oddoption
                                    oddindex += 1
                                    bidIndex += 1
                                    if bidIndex < lsize :
                                        marketline = lines[bidIndex]
                                        marketInfo = marketline.split('|')
            lIndex = bidIndex

        #for gamekey in self.games:
         #   game = self.games[gamekey]
          #  print(game.label_en+ ":")
           # for marketkey in game.markets:
            #    market = self.markets[marketkey]
             #   print("---" + market.label)

    def find_market_label(self, mlabel):
        bValid = False
        for standardlabel in self.market_labels.keys:
            if standardlabel in mlabel:
                mlabel = self.market_labels[standardlabel]
                bValid = True
                break
        if not bValid:
            return None
        else:
            return mlabel