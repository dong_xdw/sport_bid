import difflib
import GameEvent

class GamesDict(object):

    def __init__(self):
        self.footballs = {}
        self.basketballs = {}
        self.tennis = {}
        self.volleyballs = {}
        self.icehockeys = {}
        self.handballs = {}

    def find_game(self, game:GameEvent):
        glabel = game.label_en
        gtype = game.type
        if gtype == "Football":
            return self.searchMatchedGame(glabel, self.footballs)
        elif gtype == "Basketball":
            return self.searchMatchedGame(glabel, self.basketballs)
        elif gtype == "Volleyball":
            return self.searchMatchedGame(glabel, self.volleyballs)
        elif gtype == "Tennis":
            return self.searchMatchedGame(glabel, self.tennis)
        elif gtype == "IceHockey" or gtype == "Ice Hockey":
            return self.searchMatchedGame(glabel, self.icehockeys)
        elif gtype == "Handball":
            return self.searchMatchedGame(glabel, self.handballs)

    def add_game_dicts(self, game:GameEvent):
        gtype = game.type
        glabel = game.label_en
        if type == "Football":
            self.footballs[glabel] = game
        elif type == "Basketball":
            self.basketballs[glabel] = game
        elif type == "Volleyball":
            self.volleyballs[glabel] = game
        elif type == "Tennis":
            self.tennis[glabel] = game
        elif type == "Icehocky":
            self.icehockeys[glabel] = game
        elif type == "Handball":
            self.handballs[glabel] = game
        else:
            return None




    def searchMatchedGame(self, glabel, games):
        ratio, matchedgame = self.cal_gamename_ratio(glabel, games)
        if (ratio > 0.8):
            return matchedgame
        else:
            return None

    def cal_gamename_ratio(self, glabel, games):
        maxratio:float = 0.0
        maxgame:GameEvent = None
        for gname in games.keys():
            ratio = difflib.SequenceMatcher(None, glabel, gname).ratio()
            if ratio > maxratio:
                maxratio = ratio;
                maxgame = games[gname]
        return maxratio, maxgame


